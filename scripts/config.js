require("dotenv").config();

const FEE = 1e6;

const ETHUniRouter = "0x3fC91A3afd70395Cd496C647d5a6CC9D4B2b7FAD"
const SEPOLIAUniRouter = "0x3fC91A3afd70395Cd496C647d5a6CC9D4B2b7FAD"
const ARBUniRouter = "0x5E325eDA8064b456f4781070C0738d849c824258"
const AVAXUniRouter = "0x4Dae2f939ACf50408e13d58534Ff8c2776d45265"
const BASEUniRouter = "0x3fC91A3afd70395Cd496C647d5a6CC9D4B2b7FAD"
const BASESEPOLIAUniRouter = "0x050E797f3625EC8785265e1d9BDd4799b97528A1"
const OPUniRouter = "0xCb1355ff08Ab38bBCE60111F1bb2B784bE25D7e8"
const PolygonUniRouter = "0xec7BE89e9d109e7e3Fec59c222CF297125FEFda2"


// cctp domain
const ETHDomain = 0
const AVAXDomain = 1
const OPDomain = 2
const ArbitrumDomain = 3
const BaseDomain = 6
const PolygonDomain = 7


// ccpt contract
const ETHTokenMessenger = "0xbd3fa81b58ba92a82136038b25adec7066af3155"
const AVAXTokenMessenger = "0x6b25532e1060ce10cc3b0a99e5683b91bfde6982"
const OPTokenMessenger = "0x2B4069517957735bE00ceE0fadAE88a26365528f"
const ARBTokenMessenger = "0x19330d10D9Cc8751218eaf51E8885D058642E08A"
const BASETokenMessenger = "0x1682Ae6375C4E4A97e4B583BC394c861A46D8962"
const PolygonTokenMessenger = "0x9daF8c91AEFAE50b9c0E69629D3F6Ca40cA3B3FE"
const SEPOLIATokenMessenger = "0x9f3B8679c73C2Fef8b59B4f3444d4e156fb70AA5"
const BASESEPOLIATokenMessenger = "0x9f3B8679c73C2Fef8b59B4f3444d4e156fb70AA5"


// cctpMessageTransmitter
const ETHMessageTransmitter = "0x0a992d191deec32afe36203ad87d7d289a738f81"
const AVAXMessageTransmitter = "0x8186359af5f57fbb40c6b14a588d2a59c0c29880"
const OPMessageTransmitter = "0x4d41f22c5a0e5c74090899e5a8fb597a8842b3e8"
const ARBMessageTransmitter = "0xC30362313FBBA5cf9163F0bb16a0e01f01A896ca"
const BASEMessageTransmitter = "0xAD09780d193884d503182aD4588450C416D6F9D4"
const PolygonMessageTransmitter = "0xF3be9355363857F3e001be68856A2f96b4C39Ba9"
const SEPOLIAMessageTransmitter = "0x7865fAfC2db2093669d92c0F33AeEF291086BEFD"
const BASESEPOLIAMessageTransmitter = "0x7865fAfC2db2093669d92c0F33AeEF291086BEFD"


const ETHUSDC = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"
const AVAXUSDC = "0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E"
const OPUSDC = "0x0b2c639c533813f4aa9d7837caf62653d097ff85"
const ARBUSDC = "0xaf88d065e77c8cC2239327C5EDb3A432268e5831"
const BASEUSDC = "0x833589fCD6eDb6E08f4c7C32D4f71b54bdA02913"
const PolygonUSDC = "0x3c499c542cef5e3811e1192ce70d8cc03d5c3359"
const SEPOLIAUSDC = "0x1c7D4B196Cb0C7B01d743Fbc6116a902379C7238"
const BASESEPOLIAUSDC = "0x036CbD53842c5426634e7929541eC2318f3dCF7e"



module.exports = {
  FEE,
  ETHUniRouter,
  ARBUniRouter,
  AVAXUniRouter,
  BASEUniRouter,
  OPUniRouter,
  PolygonUniRouter,
  ETHDomain,
  AVAXDomain,
  OPDomain,
  ArbitrumDomain,
  BaseDomain,
  PolygonDomain,
  ETHTokenMessenger,
  AVAXTokenMessenger,
  OPTokenMessenger,
  ARBTokenMessenger,
  BASETokenMessenger,
  PolygonTokenMessenger,
  ETHMessageTransmitter,
  AVAXMessageTransmitter,
  OPMessageTransmitter,
  ARBMessageTransmitter,
  BASEMessageTransmitter,
  PolygonMessageTransmitter,
  ETHUSDC,
  AVAXUSDC,
  OPUSDC,
  ARBUSDC,
  BASEUSDC,
  PolygonUSDC,
  SEPOLIAUniRouter,
  SEPOLIATokenMessenger,
  SEPOLIAMessageTransmitter,
  SEPOLIAUSDC,
  BASESEPOLIAUniRouter,
  BASESEPOLIATokenMessenger,
  BASESEPOLIAMessageTransmitter,
  BASESEPOLIAUSDC
};
