const fs = require('fs');
require("dotenv").config();

const {
    ETHMessageTransmitter,
    AVAXMessageTransmitter,
    OPMessageTransmitter,
    ARBMessageTransmitter,
    BASEMessageTransmitter,
    PolygonMessageTransmitter,
    BASESEPOLIAMessageTransmitter,
    SEPOLIAMessageTransmitter,
} = require("./config.js");


async function setHandlerCaller(pathRInContractAddress, localHandleCaller) {
    if (pathRInContractAddress === ""){
        console.log("pathRIn Contract is not deployed.");
        return;
    }
    const [owner] = await ethers.getSigners();
    console.log("Owner Account:", owner.address);

    const PathRIn = await ethers.getContractAt("PathRSwapDestChain", pathRInContractAddress);

    await PathRIn.connect(owner).setHandlerCaller(localHandleCaller);

    const handelCaller = await PathRIn.handlerCaller();
    console.log("Current 'handelCaller' is ", handelCaller);
}

async function main() {
    const deployments = JSON.parse(fs.readFileSync('./deployment/deployments.json', 'utf8'));

    const currentNetwork = process.env.network || "unknown";

    console.log("<<<>>>>", currentNetwork);

    // Testnet Setting
    if (currentNetwork === "sepolia") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, SEPOLIAMessageTransmitter);
    }

    if (currentNetwork === "basesepolia") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, BASESEPOLIAMessageTransmitter);
    }

    // Mainnet Setting
    if (currentNetwork === "mainnet") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, ETHMessageTransmitter);
    }

    if (currentNetwork === "arbitrumOne") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, ARBMessageTransmitter);
    }

    if (currentNetwork === "base") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, BASEMessageTransmitter);
    }

    if (currentNetwork === "polygon") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, PolygonMessageTransmitter);
    }

    if (currentNetwork === "optimisticEthereum") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, OPMessageTransmitter);
    }

    if (currentNetwork === "avalanche") {
        await setHandlerCaller(deployments[currentNetwork].pathrIn, AVAXMessageTransmitter);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
