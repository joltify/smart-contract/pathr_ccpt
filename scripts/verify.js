const hre = require("hardhat");
const fs = require('fs');
require("dotenv").config();

const {
    FEE,
    ETHUniRouter,
    ARBUniRouter,
    AVAXUniRouter,
    BASEUniRouter,
    OPUniRouter,
    PolygonUniRouter,
    ETHTokenMessenger,
    AVAXTokenMessenger,
    ARBTokenMessenger,
    BASETokenMessenger,
    PolygonTokenMessenger,
    ETHMessageTransmitter,
    AVAXMessageTransmitter,
    OPMessageTransmitter,
    ARBMessageTransmitter,
    BASEMessageTransmitter,
    PolygonMessageTransmitter,
    ETHUSDC,
    AVAXUSDC,
    OPUSDC,
    ARBUSDC,
    BASEUSDC,
    PolygonUSDC,
    SEPOLIAUniRouter,
    SEPOLIATokenMessenger,
    SEPOLIAMessageTransmitter,
    SEPOLIAUSDC,
    BASESEPOLIAUniRouter,
    BASESEPOLIATokenMessenger,
    BASESEPOLIAMessageTransmitter,
    BASESEPOLIAUSDC,
    OPTokenMessenger,
} = require("./config.js");



async function doVerifyOut(pathROutContractAddress, swaprouter, tokenMessenger, messageTransmitter, usdc) {
    // verify contract
    await hre.run("verify:verify", {
        address: pathROutContractAddress,
        constructorArguments: [
            swaprouter,
            tokenMessenger,
            messageTransmitter,
            usdc,
        ],
    });
}


async function doVerifyIn(pathRInContractAddress, swaprouter, ccptcontract, usdc, stakeAsset, fee) {
    // verify contract
    await hre.run("verify:verify", {
        address: pathRInContractAddress,
        constructorArguments: [
            swaprouter,
            ccptcontract,
            usdc,
            stakeAsset,
            fee,
        ],
    });
}


async function main() {
    let deployments = JSON.parse(fs.readFileSync('./deployment/deployments.json', 'utf8'));

    const networktodeploy = process.env.network || "unknown";

    console.log("<<<>>>>", networktodeploy);

    // Testnet Deployment
    if (networktodeploy === "sepolia") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, SEPOLIAUniRouter, SEPOLIATokenMessenger, SEPOLIAUSDC, SEPOLIAUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, SEPOLIAUniRouter, SEPOLIATokenMessenger, SEPOLIAMessageTransmitter, SEPOLIAUSDC);
        return;
    }

    if (networktodeploy === "basesepolia") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, BASESEPOLIAUniRouter, BASESEPOLIATokenMessenger, BASESEPOLIAUSDC, BASESEPOLIAUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, BASESEPOLIAUniRouter, BASESEPOLIATokenMessenger, BASESEPOLIAMessageTransmitter, BASESEPOLIAUSDC);
        return;
    }

    // Mainnet Deployment
    if (networktodeploy === "arbitrumOne") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, ARBUniRouter, ARBMessageTransmitter, ARBUSDC, ARBUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, ARBUniRouter, ARBTokenMessenger, ARBMessageTransmitter, ARBUSDC);
        return;
    }


    if (networktodeploy === "base") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, BASEUniRouter, BASEMessageTransmitter, BASEUSDC, BASEUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, BASEUniRouter, BASETokenMessenger, BASEMessageTransmitter, BASEUSDC);
        return;
    }

    if (networktodeploy === "mainnet") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, ETHUniRouter, ETHMessageTransmitter, ETHUSDC, ETHUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, ETHUniRouter, ETHTokenMessenger, ETHMessageTransmitter, ETHUSDC);
        return;
    }

    if (networktodeploy === "avalanche") {
        console.log(deployments[networktodeploy].pathrIn, AVAXUniRouter, AVAXMessageTransmitter, AVAXUSDC, AVAXUSDC, FEE);
        await doVerifyIn(deployments[networktodeploy].pathrIn, AVAXUniRouter, AVAXMessageTransmitter, AVAXUSDC, AVAXUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, AVAXUniRouter, AVAXTokenMessenger, AVAXMessageTransmitter, AVAXUSDC);
        return;
    }

    if (networktodeploy === "optimisticEthereum") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, OPUniRouter, OPMessageTransmitter, OPUSDC, OPUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, OPUniRouter, OPTokenMessenger, OPMessageTransmitter, OPUSDC);
        return;
    }

    if (networktodeploy === "polygon") {
        await doVerifyIn(deployments[networktodeploy].pathrIn, PolygonUniRouter, PolygonMessageTransmitter, PolygonUSDC, PolygonUSDC, FEE);
        await doVerifyOut(deployments[networktodeploy].pathrOut, PolygonUniRouter, PolygonTokenMessenger, PolygonMessageTransmitter, PolygonUSDC);
        return;
    }

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
