const hre = require("hardhat");
const fs = require('fs');
require("dotenv").config();

const {
    FEE,
    ETHUniRouter,
    ARBUniRouter,
    AVAXUniRouter,
    BASEUniRouter,
    OPUniRouter,
    PolygonUniRouter,
    ETHTokenMessenger,
    AVAXTokenMessenger,
    OPTokenMessenger,
    ARBTokenMessenger,
    BASETokenMessenger,
    PolygonTokenMessenger,
    ETHMessageTransmitter,
    AVAXMessageTransmitter,
    OPMessageTransmitter,
    ARBMessageTransmitter,
    BASEMessageTransmitter,
    PolygonMessageTransmitter,
    ETHUSDC,
    AVAXUSDC,
    OPUSDC,
    ARBUSDC,
    BASEUSDC,
    PolygonUSDC,
    SEPOLIAUniRouter,
    SEPOLIATokenMessenger,
    SEPOLIAMessageTransmitter,
    SEPOLIAUSDC,
    BASESEPOLIAUniRouter,
    BASESEPOLIATokenMessenger,
    BASESEPOLIAMessageTransmitter,
    BASESEPOLIAUSDC
} = require("./config.js");



async function doDeployOut(swaprouter, tokenMessenger, messageTransmitter, usdc) {
    const [deployer] = await ethers.getSigners();
    console.log("Deploying contracts with the account:", deployer.address);
    console.log("Account balance:", (await deployer.getBalance()).toString());

    // deploy contract
    const ct = await ethers.getContractFactory("PathRSwapSourceChain");
    const ctdeployed = await ct.deploy(swaprouter, tokenMessenger, messageTransmitter, usdc);
    console.log("pathR outbound contract deployed to address:", ctdeployed.address);

    return ctdeployed.address;
}


async function doDeployIn(swaprouter, ccptcontract, usdc, stakeAsset, fee) {
    const [deployer] = await ethers.getSigners();
    console.log("Deploying contracts with the account:", deployer.address);
    console.log("Account balance:", (await deployer.getBalance()).toString());

    // deploy contract
    const ct = await ethers.getContractFactory("PathRSwapDestChain");
    const ctdeployed = await ct.deploy(swaprouter, ccptcontract, usdc, stakeAsset, fee);
    console.log("PathR  Inbound contract deployed to address:", ctdeployed.address);

    return ctdeployed.address;
}

async function storeDeployements(network, outContract, inContract, uniRouter, tokenMessenger, messageTransmitter, usdc, stakeAsset, fee) {
    // Read the JSON file
    let deployments = JSON.parse(fs.readFileSync('./deployment/deployments.json', 'utf8'));

    // Store deployed contract addresses
    deployments[network].pathrOut = outContract;
    deployments[network].pathrIn = inContract;
    deployments[network].uniRouter = uniRouter;
    deployments[network].tokenMessenger = tokenMessenger;
    deployments[network].messageTransmitter = messageTransmitter;
    deployments[network].usdc = usdc;
    deployments[network].stakeAsset = stakeAsset;
    deployments[network].fee = fee;

    // Convert the object to a JSON string
    const newDeployments = JSON.stringify(deployments, null, 2);

    // Write the JSON string to a file synchronously
    fs.writeFileSync('./deployment/deployments.json', newDeployments, 'utf8');
}

async function main() {

    const networktodeploy = process.env.network || "unknown";

    console.log("<<<>>>>", networktodeploy);

    // Testnet Deployment
    if (networktodeploy === "sepolia") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", SEPOLIAUniRouter, "CCTP TokenMessenger:", SEPOLIATokenMessenger, "CCTP Message transmitter", SEPOLIAMessageTransmitter, "USDC:", SEPOLIAUSDC);
        outContractAddress = await doDeployOut(SEPOLIAUniRouter, SEPOLIATokenMessenger, SEPOLIAMessageTransmitter, SEPOLIAUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", BASEUniRouter, "CCPT TokenMessager:", BASETokenMessenger, "USDC:", BASEUSDC, "Stake Asset:", BASEUSDC);
        inContractAddress = await doDeployIn(SEPOLIAUniRouter, SEPOLIATokenMessenger, SEPOLIAUSDC, SEPOLIAUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, SEPOLIAUniRouter, SEPOLIATokenMessenger, SEPOLIAMessageTransmitter, SEPOLIAUSDC, SEPOLIAUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

    if (networktodeploy === "basesepolia") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", BASESEPOLIAUniRouter, "CCTP TokenMessenger:", BASESEPOLIATokenMessenger, "CCTP Message transmitter", BASESEPOLIAMessageTransmitter, "USDC:", BASESEPOLIAUSDC);
        outContractAddress = await doDeployOut(BASESEPOLIAUniRouter, BASESEPOLIATokenMessenger, BASESEPOLIAMessageTransmitter, BASESEPOLIAUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", BASESEPOLIAUniRouter, "CCPT Transmiter:", BASESEPOLIAMessageTransmitter, "USDC:", BASESEPOLIAUSDC, "Stake Asset:", BASESEPOLIAUSDC);
        inContractAddress = await doDeployIn(BASESEPOLIAUniRouter, BASESEPOLIAMessageTransmitter, BASESEPOLIAUSDC, BASESEPOLIAUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, BASESEPOLIAUniRouter, BASESEPOLIATokenMessenger, BASESEPOLIAMessageTransmitter, BASESEPOLIAUSDC, BASESEPOLIAUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

    // Mainnet Deployment
    if (networktodeploy === "arbitrumOne") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", ARBUniRouter, "CCTP TokenMessenger:", ARBTokenMessenger, "CCTP Message transmitter", ARBMessageTransmitter, "USDC:", ARBUSDC);
        outContractAddress = await doDeployOut(ARBUniRouter, ARBTokenMessenger, ARBMessageTransmitter, ARBUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", ARBUniRouter, "CCPT Transmiter:", ARBMessageTransmitter, "USDC:", ARBUSDC, "Stake Asset:", ARBUSDC);
        inContractAddress = await doDeployIn(ARBUniRouter, ARBMessageTransmitter, ARBUSDC, ARBUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, ARBUniRouter, ARBTokenMessenger, ARBMessageTransmitter, ARBUSDC, ARBUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }


    if (networktodeploy === "base") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", BASEUniRouter, "CCTP TokenMessenger:", BASETokenMessenger, "CCTP Message transmitter", BASEMessageTransmitter, "USDC:", BASEUSDC);
        outContractAddress = await doDeployOut(BASEUniRouter, BASETokenMessenger, BASEMessageTransmitter, BASEUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", BASEUniRouter, "CCPT Transmiter:", BASEMessageTransmitter, "USDC:", BASEUSDC, "Stake Asset:", BASEUSDC);
        inContractAddress = await doDeployIn(BASEUniRouter, BASEMessageTransmitter, BASEUSDC, BASEUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, BASEUniRouter, BASETokenMessenger, BASEMessageTransmitter, BASEUSDC, BASEUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

    if (networktodeploy === "mainnet") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", ETHUniRouter, "CCTP TokenMessenger:", ETHTokenMessenger, "CCTP Message transmitter", ETHMessageTransmitter, "USDC:", ETHUSDC);
        outContractAddress = await doDeployOut(ETHUniRouter, ETHTokenMessenger, ETHMessageTransmitter, ETHUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", ETHUniRouter, "CCPT Transmiter:", ETHMessageTransmitter, "USDC:", ETHUSDC, "Stake Asset:", ETHUSDC);
        inContractAddress = await doDeployIn(ETHUniRouter, ETHMessageTransmitter, ETHUSDC, ETHUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, ETHUniRouter, ETHTokenMessenger, ETHMessageTransmitter, ETHUSDC, ETHUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

    if (networktodeploy === "avalanche") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", AVAXUniRouter, "CCTP TokenMessenger:", AVAXTokenMessenger, "CCTP Message transmitter", AVAXMessageTransmitter, "USDC:", AVAXUSDC);
        outContractAddress = await doDeployOut(AVAXUniRouter, AVAXTokenMessenger, AVAXMessageTransmitter, AVAXUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", AVAXUniRouter, "CCPT Transmiter:", AVAXMessageTransmitter, "USDC:", AVAXUSDC, "Stake Asset:", AVAXUSDC);
        inContractAddress = await doDeployIn(AVAXUniRouter, AVAXMessageTransmitter, AVAXUSDC, AVAXUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, AVAXUniRouter, AVAXTokenMessenger, AVAXMessageTransmitter, AVAXUSDC, AVAXUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

    if (networktodeploy === "optimisticEthereum") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", OPUniRouter, "CCTP TokenMessenger:", OPTokenMessenger, "CCTP Message transmitter", OPMessageTransmitter, "USDC:", OPUSDC);
        outContractAddress = await doDeployOut(OPUniRouter, OPTokenMessenger, OPMessageTransmitter, OPUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", OPUniRouter, "CCPT Transmiter:", OPMessageTransmitter, "USDC:", OPUSDC, "Stake Asset:", OPUSDC);
        inContractAddress = await doDeployIn(OPUniRouter, OPMessageTransmitter, OPUSDC, OPUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, OPUniRouter, OPTokenMessenger, OPMessageTransmitter, OPUSDC, OPUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

    if (networktodeploy === "polygon") {
        // Deploying PathR Outbound contract
        console.log("universal rounter address:", PolygonUniRouter, "CCTP TokenMessenger:", PolygonTokenMessenger, "CCTP Message transmitter", PolygonMessageTransmitter, "USDC:", PolygonUSDC);
        outContractAddress = await doDeployOut(PolygonUniRouter, PolygonTokenMessenger, PolygonMessageTransmitter, PolygonUSDC);
        console.log("network ", networktodeploy, " >>>deployment outbound contract done");

        console.log("----------------------------------------------------------");

        // Deploying PathR Inbound contract
        console.log("universal rounter address:", PolygonUniRouter, "CCPT Transmiter:", PolygonMessageTransmitter, "USDC:", PolygonUSDC, "Stake Asset:", PolygonUSDC);
        inContractAddress = await doDeployIn(PolygonUniRouter, PolygonMessageTransmitter, PolygonUSDC, PolygonUSDC, FEE);
        console.log("network ", networktodeploy, " >>> deployment inbound contract done");

        // Store deployed contract addresses
        await storeDeployements(networktodeploy, outContractAddress, inContractAddress, PolygonUniRouter, PolygonTokenMessenger, PolygonMessageTransmitter, PolygonUSDC, PolygonUSDC, FEE);
        console.log("Store deployed contracts done");
        return;
    }

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
