const fs = require('fs');
require("dotenv").config();

const {
    ETHDomain,
    AVAXDomain,
    OPDomain,
    ArbitrumDomain,
    BaseDomain,
    PolygonDomain,
} = require("./config.js");


async function addRemoteTokenMessenger(pathRInContractAddress, domain, remoteTokenMessenger) {
    if (remoteTokenMessenger === "0x000000000000000000000000") {
        console.log("remoteTokenMessager is empty for domain ", domain);
        return;
    }

    const [owner] = await ethers.getSigners();
    console.log("Owner Account:", owner.address);
    // console.log("Account balance:", (await owner.getBalance()).toString());

    const PathRIn = await ethers.getContractAt("PathRSwapDestChain", pathRInContractAddress);

    await PathRIn.connect(owner).addRemoteTokenMessenger(domain, remoteTokenMessenger);

    const remoteTokenMessager = await PathRIn.remoteTokenMessengers(domain);
    console.log("Current 'remoteTokenMessager' on domain ", domain, " is ", remoteTokenMessager);
}


async function main() {
    const deployments = JSON.parse(fs.readFileSync('./deployment/deployments.json', 'utf8'));

    const currentNetwork = process.env.network || "unknown";

    console.log("<<<>>>>", currentNetwork);

    // Testnet Setting
    if (currentNetwork === "sepolia") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, BaseDomain, "0x000000000000000000000000" + deployments.basesepolia.pathrOut.slice(2));
    }

    if (currentNetwork === "basesepolia") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, ETHDomain, "0x000000000000000000000000" + deployments.sepolia.pathrOut.slice(2));
    }

    // Mainnet Setting
    if (currentNetwork === "mainnet") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, ArbitrumDomain, "0x000000000000000000000000" + deployments.arbitrumOne.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, BaseDomain, "0x000000000000000000000000" + deployments.base.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, PolygonDomain, "0x000000000000000000000000" + deployments.polygon.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, OPDomain, "0x000000000000000000000000" + deployments.optimisticEthereum.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, AVAXDomain, "0x000000000000000000000000" + deployments.avalanche.pathrOut.slice(2));
    }

    if (currentNetwork === "arbitrumOne") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, BaseDomain, "0x000000000000000000000000" + deployments.base.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, PolygonDomain, "0x000000000000000000000000" + deployments.polygon.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, OPDomain, "0x000000000000000000000000" + deployments.optimisticEthereum.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, AVAXDomain, "0x000000000000000000000000" + deployments.avalanche.pathrOut.slice(2));
    }

    if (currentNetwork === "base") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, ArbitrumDomain, "0x000000000000000000000000" + deployments.arbitrumOne.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, PolygonDomain, "0x000000000000000000000000" + deployments.polygon.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, OPDomain, "0x000000000000000000000000" + deployments.optimisticEthereum.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, AVAXDomain, "0x000000000000000000000000" + deployments.avalanche.pathrOut.slice(2));
    }

    if (currentNetwork === "polygon") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, ArbitrumDomain, "0x000000000000000000000000" + deployments.arbitrumOne.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, BaseDomain, "0x000000000000000000000000" + deployments.base.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, OPDomain, "0x000000000000000000000000" + deployments.optimisticEthereum.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, AVAXDomain, "0x000000000000000000000000" + deployments.avalanche.pathrOut.slice(2));
    }

    if (currentNetwork === "optimisticEthereum") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, ArbitrumDomain, "0x000000000000000000000000" + deployments.arbitrumOne.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, BaseDomain, "0x000000000000000000000000" + deployments.base.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, PolygonDomain, "0x000000000000000000000000" + deployments.polygon.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, AVAXDomain, "0x000000000000000000000000" + deployments.avalanche.pathrOut.slice(2));
    }

    if (currentNetwork === "avalanche") {
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, ArbitrumDomain, "0x000000000000000000000000" + deployments.arbitrumOne.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, BaseDomain, "0x000000000000000000000000" + deployments.base.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, PolygonDomain, "0x000000000000000000000000" + deployments.polygon.pathrOut.slice(2));
        await addRemoteTokenMessenger(deployments[currentNetwork].pathrIn, OPDomain, "0x000000000000000000000000" + deployments.optimisticEthereum.pathrOut.slice(2));
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
