const fs = require('fs');
require("dotenv").config();

const {
    ETHUSDC,
    AVAXUSDC,
    OPUSDC,
    ARBUSDC,
    BASEUSDC,
    PolygonUSDC,
    SEPOLIAUSDC,
    BASESEPOLIAUSDC,
} = require("./config.js");


async function addToWhiteList(pathRInContractAddress, stakerAddress) {
    if (pathRInContractAddress === ""){
        console.log("pathRIn Contract is not deployed.");
        return;
    }
    const [owner] = await ethers.getSigners();
    console.log("Owner Account:", owner.address);

    const PathRIn = await ethers.getContractAt("PathRSwapDestChain", pathRInContractAddress);

    await PathRIn.connect(owner).addToWhiteList(stakerAddress);

    const whitelist = await PathRIn.getWhiteList();
    console.log("Current 'isWhiteListed' is ", whitelist);

}

async function stake(pathRInContractAddress, usdcAddress, amount) {
    if (pathRInContractAddress === ""){
        console.log("pathRIn Contract is not deployed.");
        return;
    }
    const [owner] = await ethers.getSigners();
    console.log("Owner Account:", owner.address);
    console.log("Staker Account:", owner.address);
    const usdc = await ethers.getContractAt("IERC20", usdcAddress);
    const PathRIn = await ethers.getContractAt("PathRSwapDestChain", pathRInContractAddress);

    await usdc.connect(owner).approve(pathRInContractAddress, amount);
    await PathRIn.connect(owner).stakeTokens(amount);

    const stakedAmount = await PathRIn.getStakeBalance(owner.address);
    console.log("Current 'stakedAmount' is ", stakedAmount);
}

async function main() {
    const deployments = JSON.parse(fs.readFileSync('./deployment/deployments.json', 'utf8'));

    const currentNetwork = process.env.network || "unknown";

    const [owner] = await ethers.getSigners();

    console.log("<<<>>>>", currentNetwork);

    // Testnet Setting
    if (currentNetwork === "sepolia") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, SEPOLIAUSDC, 1);
    }

    if (currentNetwork === "basesepolia") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, BASESEPOLIAUSDC, 1);
    }

    // Mainnet Setting
    if (currentNetwork === "mainnet") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, ETHUSDC, 1);
    }

    if (currentNetwork === "arbitrumOne") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, ARBUSDC, 1);
    }

    if (currentNetwork === "base") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, BASEUSDC, 1);
    }

    if (currentNetwork === "polygon") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, PolygonUSDC, 1);
    }

    if (currentNetwork === "optimisticEthereum") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, OPUSDC, 1);
    }

    if (currentNetwork === "avalanche") {
        await addToWhiteList(deployments[currentNetwork].pathrIn, owner.address);
        await stake(deployments[currentNetwork].pathrIn, AVAXUSDC, 1);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
