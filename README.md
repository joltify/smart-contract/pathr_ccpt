<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

- npm

  ```sh
  npm install npm@latest -g
  ```

- hardhat

  ```sh
  npm install --save-dev hardhat
  ```

  run:

  ```sh
   npx hardhat node --fork https://eth-mainnet.g.alchemy.com/v2/ALCHEMY_API_KEY
  ```


### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/Aboudoc/Uniswap-v2.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Dependencies

   ```sh
   npm i @uniswap/v2-core @uniswap/v2-periphery
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

If you need testnet funds, use the [Alchemy testnet faucet](https://goerlifaucet.com/).

**This project shows how to swap, add and remove liquidity**


## Forking mainnet

When we fork 🍴 the mainnet, we have the current state of the blockchain running locally on our system, including all contracts deployed on it and all transactions performed on it.

1. Setup hardhat.config
2. Find a whale on etherscan

`hardhat.config.js`

```sh
  networks: {
        hardhat: {
          forking: {
            url: `https://eth-mainnet.alchemyapi.io/v2/${process.env.ALCHEMY_API_KEY}`,
       },
     },
  }
```
Note: Replace the `${}` component of the URL with your personal [Alchemy](https://www.alchemy.com/) API key.
`.config`

```js
const DAI = "0x6B175474E89094C44Da98b954EedeAC495271d0F";
const DAI_WHALE = process.env.DAI_WHALE;

module.exports = {
  DAI,
  DAI_WHALE,
};
```

`.env`

```sh
ALCHEMY_API_KEY=...
```

`Terminal 1`

```sh
ALCHEMY_API_KEY=...
npx hardhat node --fork https://eth-mainnet.g.alchemy.com/v2/$ALCHEMY_API_KEY
```


`Terminal 2`

```sh
npx hardhat test 
npx hardhat coverage
```

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

