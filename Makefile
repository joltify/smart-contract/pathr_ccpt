format:
	@npx prettier --write --plugin=prettier-plugin-solidity 'contracts/**/*.sol'
.PHONY: format

lint:
	 npx solhint -f table contracts/**/*.sol
.PHONY: lint
