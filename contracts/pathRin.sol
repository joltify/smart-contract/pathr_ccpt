// SPDX-License-Identifier: MIT
pragma solidity 0.8.20;
pragma abicoder v2;

import "@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol";
import "@uniswap/universal-router/contracts/interfaces/IUniversalRouter.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable2Step.sol";
import "./cctp_messages/IReceiver.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./cctp_messages/Message.sol";
import "./cctp_messages/BurnMessage.sol";
import "./interface/IMessageHandler.sol";

contract PathRSwapDestChain is Ownable2Step, ReentrancyGuard, IMessageHandler {
    // ============ Libraries ============
    using TypedMemView for bytes;
    using TypedMemView for bytes29;
    using BurnMessage for bytes29;
    using Message for bytes29;
    using BurnMessage for bytes29;

    // For the scope of these swap examples,
    // we will detail the design considerations when using
    // `exactInput`, `exactInputSingle`, `exactOutput`, and  `exactOutputSingle`.

    // It should be noted that for the sake of these examples, we purposefully pass in the swap router instead of inherit the swap router for simplicity.
    // More advanced example contracts will detail how to inherit the swap router safely.

    // these are the contract init configure
    using SafeERC20 for IERC20;

    struct Account {
        address addr;
        uint256 balance;
    }

    Account[] public relayers;
    address[] internal _whitelistRelayer;

    // we add the stake token and variables
    IERC20 public immutable stakingContract;
    mapping(address => uint256) public balances;

    IUniversalRouter public uniRouter;
    uint256 public fee;
    address public immutable USDC;
    IReceiver public immutable cctpMessagerTransmiter;
    address public handlerCaller;
    address private _contractAddress;
    uint256 public constant activeRelayerCount = 6;
    uint256 public constant maxValidators = 20;

    // Events
    event tokensStaked(address indexed from, uint256 amount);
    event TokensUnstaked(address indexed to, uint256 amount);
    event HandleReceiveMessage(
        uint256,
        address indexed,
        address indexed,
        address indexed
    );
    event SetFee(uint256);
    event SetHandlerCaller(address indexed);
    event SwapExactInputSingle(
        uint256,
        uint256,
        address indexed,
        address indexed
    );
    event UpdateRelayers(address indexed, uint256);
    event RemoveFromWhiteList(address indexed);
    event RemoveFromRelayers(address indexed);

    /**
     * @notice Emitted when a remote TokenMessenger is added
     * @param domain remote domain
     * @param tokenMessenger TokenMessenger on remote domain
     */
    event RemoteTokenMessengerAdded(uint32 domain, bytes32 tokenMessenger);

    /**
     * @notice Emitted when a remote TokenMessenger is removed
     * @param domain remote domain
     * @param tokenMessenger TokenMessenger on remote domain
     */
    event RemoteTokenMessengerRemoved(uint32 domain, bytes32 tokenMessenger);

    event MintAndSwap(uint256 inAmt, address outToken, address receiver);
    // This example swaps DAI/WETH9 for single path swaps and DAI/USDC/WETH9 for multi path swaps.

    event PathRSwap(address, address, uint, uint);

    // Valid TokenMessengers on remote domains
    mapping(uint32 => bytes32) public remoteTokenMessengers;

    // ============ Modifiers ============
    /**
     * @notice Only accept messages from a registered TokenMessenger contract on given remote domain
     * @param domain The remote domain
     * @param tokenMessenger The address of the TokenMessenger contract for the given remote domain
     */
    modifier onlyRemoteTokenMessenger(uint32 domain, bytes32 tokenMessenger) {
        require(
            _isRemoteTokenMessenger(domain, tokenMessenger),
            "Remote TokenMessenger unsupported"
        );
        _;
    }

    /**
     * @notice Only accept messages from the registered message transmitter on local domain
     */
    modifier onlyLocalMessageTransmitter() {
        // Caller must be the registered message transmitter for this domain
        require(_isLocalMessageTransmitter(), "Invalid message transmitter");
        _;
    }

    constructor(
        address _unirouter,
        address _messageTransmitter,
        address _usdc,
        address _stakingContract,
        uint256 _fee
    ) Ownable() {
        uniRouter = IUniversalRouter(_unirouter);
        USDC = address(_usdc);
        cctpMessagerTransmiter = IReceiver(_messageTransmitter);
        stakingContract = IERC20(_stakingContract);
        fee = _fee;
        _contractAddress = address(this);
    }

    /**
     * @notice Returns true if the message sender is the local registered MessageTransmitter
     * @return true if message sender is the registered local message transmitter
     */
    function _isLocalHandlerCaller() internal view returns (bool) {
        address localHandlerCaller = handlerCaller;
        return
            address(localHandlerCaller) != address(0) &&
            msg.sender == address(localHandlerCaller);
    }

    function setFee(uint256 _fee) public onlyOwner {
        fee = _fee;
        emit SetFee(_fee);
    }

    function setHandlerCaller(address _handlerCaller) public onlyOwner {
        handlerCaller = _handlerCaller;
        emit SetHandlerCaller(_handlerCaller);
    }

    function setRouter(IUniversalRouter _router) public onlyOwner {
        uniRouter = _router;
    }

    /**
     * @notice handles an incoming message from a Receiver
     * @param sourceDomain the source domain of the message
     * @param sender the sender of the message
     * @param messageBody The message raw bytes
     * @return success bool, true if successful
     */
    function handleReceiveMessage(
        uint32 sourceDomain,
        bytes32 sender,
        bytes calldata messageBody
    )
        external
        override
        onlyLocalMessageTransmitter
        onlyRemoteTokenMessenger(sourceDomain, sender)
        returns (bool)
    {
        uint256 lFee = fee;
        bytes29 _msg = messageBody.ref(0);
        _msg._validateSwapMessageFormat();

        uint256 _amount = _msg._getAmountSent();
        uint24 _chainFee = _msg._getPoolFee();
        address _receiver = Message.bytes32ToAddress(_msg._getReceiver());
        address _targetToken = Message.bytes32ToAddress(
            _msg._getSentTokenContract()
        );

        uint amtToSwap = _amount;
        require(amtToSwap > lFee, "insufficient fee");

        address _currentRelayer = getCurrentRelayer();
        
        emit HandleReceiveMessage(_amount, _receiver, _targetToken, _currentRelayer);

        if (lFee != 0) {
            TransferHelper.safeTransfer(USDC, _currentRelayer, lFee);
            amtToSwap = _amount - lFee;
        }

        if (_targetToken == USDC) {
            TransferHelper.safeTransfer(USDC, _receiver, amtToSwap);
            return true;
        } else {
            _swapExactInputSingle(
                amtToSwap,
                _chainFee,
                _targetToken,
                _receiver
            );
            emit MintAndSwap(amtToSwap, _targetToken, _receiver);
            return true;
        }
    }

    function mintandswap(
        bytes calldata _message1,
        bytes calldata _attestation1,
        bytes calldata _message2,
        bytes calldata _attestation2
    ) external nonReentrant {
        address supposerelayer = getCurrentRelayer();
        require(supposerelayer == msg.sender, "not the supposed relayer");

        bool ok = cctpMessagerTransmiter.receiveMessage(
            _message1,
            _attestation1
        );
        require(ok, "cctp mint token failed");

        ok = cctpMessagerTransmiter.receiveMessage(_message2, _attestation2);

        require(ok, "cctp process swap failed");
    }

    function _swapExactInputSingle(
        uint256 _amountIn,
        uint24 _poolFee,
        address _outToken,
        address _receiver
    ) internal {
        TransferHelper.safeTransfer(USDC, address(uniRouter), _amountIn);

        // Create the command
        bytes memory commands = new bytes(1);
        commands[0] = 0x00; // The command value for V3_SWAP_EXACT_IN

        // Create the UniswapV3 encoded path
        bytes memory path = abi.encodePacked(USDC, _poolFee, _outToken);

        // Create the array of inputs
        bytes[] memory inputs = new bytes[](1);
        inputs[0] = abi.encode(_receiver, _amountIn, 0, path, false);

        // Execute the swap using the universal router
        uniRouter.execute(commands, inputs, block.timestamp + 15);
    }

    /// @param amount to allocate to recipient.
    function stakeTokens(uint256 amount) public nonReentrant {
        require(amount != 0, "stake amount should be greater than 0");
        bool isrelayer = _checkInWhiteList(_whitelistRelayer, msg.sender);
        require(isrelayer, "not in the relayer list");
        require(
            amount <= stakingContract.balanceOf(msg.sender),
            "Not enough STAKE tokens in your wallet, please try lesser amount"
        );
        stakingContract.safeTransferFrom(msg.sender, _contractAddress, amount);
        balances[msg.sender] = balances[msg.sender] + amount;
        _updateRelayers(msg.sender, true);
        emit tokensStaked(msg.sender, amount);
    }

    /// @param amount - the amount to unlock (in wei)
    function unstakeTokens(uint256 amount) public nonReentrant {
        uint256 myBalance = balances[msg.sender];
        require(
            myBalance >= amount,
            "Insufficient token balance, try lesser amount"
        );
        balances[msg.sender] = myBalance - amount;
        stakingContract.safeTransfer(msg.sender, amount);
        _updateRelayers(msg.sender, false);
        emit TokensUnstaked(msg.sender, amount);
    }

    // Function to update the relayers array
    function _updateRelayers(address _addr, bool isStake) internal {
        // Check if the account already exists in relayers
        bool exists = false;
        uint256 accountsNum = relayers.length;
        require(accountsNum < maxValidators, "we only support 20 validators");
        for (uint256 i = 0; i < accountsNum; i++) {
            if (relayers[i].addr == _addr) {
                relayers[i].balance = balances[_addr];
                exists = true;
                break;
            }
        }

        // If it doesn't exist, add it
        if ((!exists) && isStake) {
            relayers.push(Account({addr: _addr, balance: balances[_addr]}));
        }

        // Sort the relayers array
        _sortRelayers();

        emit UpdateRelayers(_addr, balances[_addr]);
    }

    // Function to sort the topAccounts array
    function _sortRelayers() internal {
        uint256 length = relayers.length;
        for (uint256 i = 0; i < length; i++) {
            for (uint256 j = i + 1; j < length; j++) {
                if (relayers[i].balance < relayers[j].balance) {
                    Account memory temp = relayers[i];
                    relayers[i] = relayers[j];
                    relayers[j] = temp;
                }
            }
        }
    }

    // Function to get the active relayers
    function getActiveRelayers() public view returns (Account[] memory) {
        Account[] memory _activeRelayers;
        uint256 counter = activeRelayerCount;
        if (relayers.length < activeRelayerCount) {
            counter = relayers.length;
        }

        _activeRelayers = new Account[](counter);
        for (uint256 i = 0; i < counter; i++) {
            _activeRelayers[i] = relayers[i];
        }

        return _activeRelayers;
    }

    // Function to get the balance of an account
    function getStakeBalance(address _addr) public view returns (uint256) {
        return balances[_addr];
    }

    function getWhiteList() public view returns (address[] memory) {
        return _whitelistRelayer;
    }

    function addToWhiteList(address relayer) public onlyOwner {
        require(
            !_checkInWhiteList(_whitelistRelayer, relayer),
            "relayer already in the list"
        );
        require(_whitelistRelayer.length < maxValidators, "max validators");
        // Append to array
        // This will increase the array length by 1.
        _whitelistRelayer.push(relayer);
    }

    function removeFromWhiteList(uint256 index) public onlyOwner {
        require(index < _whitelistRelayer.length, "index out of bounds");
        uint256 length = _whitelistRelayer.length;
        address toBeRemoved = _whitelistRelayer[index];

        // remove from relayers list
        for (uint256 i = 0; i < relayers.length; i++) {
            if (relayers[i].addr == toBeRemoved) {
                relayers[i] = relayers[relayers.length - 1];
                relayers.pop();
                emit RemoveFromRelayers(toBeRemoved);
                break;
            }
        }

        _sortRelayers();

        if (length == 1) {
            _whitelistRelayer.pop();
            emit RemoveFromWhiteList(toBeRemoved);
            return;
        }

        _whitelistRelayer[index] = _whitelistRelayer[length - 1];
        _whitelistRelayer.pop();
        emit RemoveFromWhiteList(toBeRemoved);
        return;
    }

    function getCurrentRelayer() public view returns (address) {
        Account[] memory _activeRelayers = getActiveRelayers();
        uint256 length = _activeRelayers.length;
        require(length > 0, "no relayer in the list");
        // we choose another relayer every 100 blocks
        uint256 adjustIndex = (block.number % 1000) / 100;
        uint256 index = adjustIndex % length;
        return _activeRelayers[index].addr;
    }

    function _checkInWhiteList(
        address[] memory addressArray,
        address target
    ) internal pure returns (bool) {
        bool found = false;
        uint256 length = addressArray.length;
        for (uint256 i = 0; i < length; i++) {
            if (addressArray[i] == target) {
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     * @notice Add the TokenMessenger for a remote domain.
     * @dev Reverts if there is already a TokenMessenger set for domain.
     * @param domain Domain of remote TokenMessenger.
     * @param tokenMessenger Address of remote TokenMessenger as bytes32.
     */
    function addRemoteTokenMessenger(
        uint32 domain,
        bytes32 tokenMessenger
    ) external onlyOwner {
        require(tokenMessenger != bytes32(0), "bytes32(0) not allowed");

        require(
            remoteTokenMessengers[domain] == bytes32(0),
            "TokenMessenger already set"
        );

        remoteTokenMessengers[domain] = tokenMessenger;
        emit RemoteTokenMessengerAdded(domain, tokenMessenger);
    }

    /**
     * @notice Remove the TokenMessenger for a remote domain.
     * @dev Reverts if there is no TokenMessenger set for `domain`.
     * @param domain Domain of remote TokenMessenger
     */
    function removeRemoteTokenMessenger(uint32 domain) external onlyOwner {
        // No TokenMessenger set for given remote domain.
        require(
            remoteTokenMessengers[domain] != bytes32(0),
            "No TokenMessenger set"
        );

        bytes32 _removedTokenMessenger = remoteTokenMessengers[domain];
        delete remoteTokenMessengers[domain];
        emit RemoteTokenMessengerRemoved(domain, _removedTokenMessenger);
    }

    /**
     * @notice Return true if the given remote domain and TokenMessenger is registered
     * on this TokenMessenger.
     * @param _domain The remote domain of the message.
     * @param _tokenMessenger The address of the TokenMessenger on remote domain.
     * @return true if a remote TokenMessenger is registered for `_domain` and `_tokenMessenger`,
     * on this TokenMessenger.
     */
    function _isRemoteTokenMessenger(
        uint32 _domain,
        bytes32 _tokenMessenger
    ) internal view returns (bool) {
        return
            _tokenMessenger != bytes32(0) &&
            remoteTokenMessengers[_domain] == _tokenMessenger;
    }

    /**
     * @notice Returns true if the message sender is the local registered MessageTransmitter
     * @return true if message sender is the registered local message transmitter
     */
    function _isLocalMessageTransmitter() internal view returns (bool) {
        return
            address(cctpMessagerTransmiter) != address(0) &&
            msg.sender == address(cctpMessagerTransmiter);
    }
}
