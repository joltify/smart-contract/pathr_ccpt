// SPDX-License-Identifier: MIT
/*
 * Copyright (c) 2022, Circle Internet Financial Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

pragma solidity 0.8.20;

import "./TypedMemView.sol";

library BurnMessage {
    using TypedMemView for bytes;
    using TypedMemView for bytes29;

    // here is the info for the swap message
    //4 bytes target chain, 3byes fee, 32 bytes amount+ 32bytes receiver + 32bytes outToken
    uint8 private constant _SWAP_MESSAGE_LEN = 103;
    uint8 private constant _TARGET_POOL_FEE = 4;
    uint8 private constant _MSG_TARGET_POOL_FEE_LEN = 3;
    uint8 private constant _SWAP_AMOUNT_INDEX = 7;
    uint8 private constant _MSG_SWAP_AMOUNT_LEN = 32;
    uint8 private constant _RECEIVER_INDEX = 39;
    uint8 private constant _MSG_RECEIVER_LEN = 32;
    uint8 private constant _OUTTOKEN_INDEX = 71;
    uint8 private constant _MSG_OUTTOKEN_LEN = 32;

    /**
     * @notice FormatSwapMessage Burn message
     * @param _amountSent The burn amount
     * @param _receiver The receiver address on destination domain as bytes32
     * @param _outToken The token address on destination domain as bytes32
     * @return Burn swap formatted message.
     */
    function _formatSwapMessage(
        uint32 _targetChain,
        uint24 _targetPoolFee,
        uint256 _amountSent,
        bytes32 _receiver,
        bytes32 _outToken
    ) internal pure returns (bytes memory) {
        return
            abi.encodePacked(
                _targetChain,
                _targetPoolFee,
                _amountSent,
                _receiver,
                _outToken
            );
    }

    /**
     * @notice Retrieves the chainFee
     * @param _message The message
     * @return fee as uint24
     */
    function _getPoolFee(bytes29 _message) internal pure returns (uint24) {
        return
            uint24(
                _message.indexUint(_TARGET_POOL_FEE, _MSG_TARGET_POOL_FEE_LEN)
            );
    }

    /**
     * @notice Retrieves the amount of swap message
     * @param _message The message
     * @return sourceToken address as uint256
     */
    function _getAmountSent(bytes29 _message) internal pure returns (uint256) {
        return _message.indexUint(_SWAP_AMOUNT_INDEX, _MSG_SWAP_AMOUNT_LEN);
    }

    /**
     * @notice Retrieves the receiver of swap message
     * @param _message The message
     * @return sourceToken address as uint256
     */
    function _getReceiver(bytes29 _message) internal pure returns (bytes32) {
        return _message.index(_RECEIVER_INDEX, _MSG_RECEIVER_LEN);
    }

    /**
     * @notice Retrieves the out token contract of swap message
     * @param _message The message
     * @return sourceToken address as uint256
     */
    function _getSentTokenContract(
        bytes29 _message
    ) internal pure returns (bytes32) {
        return _message.index(_OUTTOKEN_INDEX, _MSG_OUTTOKEN_LEN);
    }

    /**
     * @notice Reverts if burn message is malformed or invalid length
     * @param _message The swap message as bytes29
     */
    function _validateSwapMessageFormat(bytes29 _message) internal pure {
        require(_message.isValid(), "Malformed message");
        require(
            _message.len() == _SWAP_MESSAGE_LEN,
            "Invalid swap message length"
        );
    }
}
