// SPDX-License-Identifier: MIT
pragma solidity 0.8.20;
pragma abicoder v2;

import "@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol";
import "@uniswap/universal-router/contracts/interfaces/IUniversalRouter.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable2Step.sol";
import "./interface/ITokenMessenger.sol";
import "./interface/IMessageTransmitter.sol";
import "./cctp_messages/BurnMessage.sol";
import "./cctp_messages/Message.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract PathRSwapSourceChain is Ownable2Step, ReentrancyGuard {
    using Message for bytes29;
    using BurnMessage for bytes29;
    using SafeERC20 for IERC20;

    IUniversalRouter public uniRouter;
    address public USDC;
    ITokenMessenger public immutable cctpTokenMessenger;
    IMessageTransmitter public immutable localMessageTransmitter;

    address private immutable _contractAddress;

    // event for logging the swap
    event PathRSwap(address indexed, address, uint, uint, address);
    event SetRouter(address indexed);
    event MydepositAndBurn(uint256, uint32, address indexed);

    constructor(
        address _unirouter,
        address _ccptokenMessenger,
        address _messageTransmitter,
        address _usdc
    ) Ownable() {
        uniRouter = IUniversalRouter(_unirouter);
        USDC = address(_usdc);
        cctpTokenMessenger = ITokenMessenger(address(_ccptokenMessenger));
        localMessageTransmitter = IMessageTransmitter(_messageTransmitter);
        _contractAddress = address(this);
    }

    function setRouter(IUniversalRouter _router) public onlyOwner {
        uniRouter = _router;
        emit SetRouter(address(_router));
    }

    // ============ External Functions  ============
    /**
     * @notice swap the token to usdc and call the cctp
     * @param _amountIn the amount of token to swap
     * @param _inToken the token to swap
     * @param _outToken the token contract to receive
     * @param _targetChain the chain to receive the token
     * @param _receiver the receiver address
     * @param _receiverContract the receiver contract address
     */
    function swapExactInputSingle(
        uint256 _amountIn,
        address _inToken,
        address _outToken,
        uint24 _poolFee,
        uint24 _destPoolFee,
        uint32 _targetChain,
        address _receiver,
        address _receiverContract
    ) external nonReentrant {
        uint256 amountOutUSDC;
        uint64 nonce;

        // Naively set amountOutMinimum to 0. In production, use an oracle or other data source to choose a safer value for amountOutMinimum.
        // We also set the sqrtPriceLimitx96 to be 0 to ensure we swap our exact input amount.

        if (_inToken != USDC) {
            TransferHelper.safeTransferFrom(
                _inToken,
                msg.sender,
                address(uniRouter),
                _amountIn
            );

            // Create the command
            bytes memory commands = new bytes(1);

            commands[0] = 0x00; // The command value for V3_SWAP_EXACT_IN

            // Create the UniswapV3 encoded path
            bytes memory path = abi.encodePacked(_inToken, _poolFee, USDC);

            // Create the array of inputs
            bytes[] memory inputs = new bytes[](1);
            inputs[0] = abi.encode(_contractAddress, _amountIn, 0, path, false);

            // Execute the swap using the universal router
            uniRouter.execute(commands, inputs, block.timestamp + 15);

            amountOutUSDC = IERC20(USDC).balanceOf(address(this));
        } else {
            TransferHelper.safeTransferFrom(
                _inToken,
                msg.sender,
                _contractAddress,
                _amountIn
            );

            amountOutUSDC = _amountIn;
        }

        nonce = _myDepositAndBurn(
            amountOutUSDC,
            _targetChain,
            _receiverContract
        );

        bytes memory _burnSwapMessage = BurnMessage._formatSwapMessage(
            _targetChain,
            _destPoolFee,
            amountOutUSDC,
            Message.addressToBytes32(_receiver),
            Message.addressToBytes32(_outToken)
        );

        localMessageTransmitter.sendMessage(
            _targetChain,
            Message.addressToBytes32(_receiverContract),
            _burnSwapMessage
        );

        require(nonce != 0, "invalid cctp operation");
        emit PathRSwap(_inToken, _outToken, _amountIn, _targetChain, _receiver);
    }

    function _myDepositAndBurn(
        uint256 _amount,
        uint32 _targetchain,
        address _receiverContract
    ) internal returns (uint64 _nonce) {
        TransferHelper.safeApprove(USDC, address(cctpTokenMessenger), _amount);
        emit MydepositAndBurn(_amount, _targetchain, _receiverContract);
        return
            cctpTokenMessenger.depositForBurnWithCaller(
                _amount,
                _targetchain,
                Message.addressToBytes32(_receiverContract),
                USDC,
                Message.addressToBytes32(_receiverContract)
            );
    }
}
