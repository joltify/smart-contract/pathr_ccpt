// const { BigNumber } = require("@ethersproject/bignumber");
const { assert, expect } = require("chai");
const { ethers } = require("hardhat");
const {
  WETH_WHALE,
  DAI_WHALE,
  WETHDAI_WHALE,
  WETH_WHALE1,
  WETH,
  DAI,
  CRV,
  RouterETH,
  CCTPETH,
  USDC,
  USDT,
  USDC_WHALE,
  USDT_WHALE,
} = require("./config.js");

// Single Hop Swap ✅
describe("PathR destination chain test", function () {
  let TestPathRInContract;

  beforeEach(async () => {
    const TestPathRIn = await ethers.getContractFactory("PathRSwapDestChain");
    TestPathRInContract = await TestPathRIn.deploy(
      RouterETH,
      CCTPETH,
      USDC,
      USDC,
        0,
    );
    await TestPathRInContract.deployed();
    console.log("TestPathRIn deployed to:", TestPathRInContract.address);
    const owner = await TestPathRInContract.owner();
    console.log("Owner of the contract:", owner);
  });

  it("should stake failed as invalid token", async () => {
    await hre.network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [USDT_WHALE],
    });

    const impersonateSigner = await ethers.getSigner(USDT_WHALE);
    const USDTContract = await ethers.getContractAt("IERC20", USDT);

    const user1Balance = await USDTContract.balanceOf(
      impersonateSigner.address
    );
    console.log(impersonateSigner.address);
    console.log(
      "Initial USDC Balance:",
      ethers.utils.formatUnits(user1Balance, 6)
    );

    await USDTContract.connect(impersonateSigner).approve(
      TestPathRInContract.address,
      user1Balance
    );

    await expect(
      TestPathRInContract.connect(impersonateSigner).stakeTokens(USDT, 1)
    ).to.be.revertedWith(
      "You are only allowed to stake the token address which was passed into this contract's constructor"
    );
  });


  it("authority check", async () => {

    const [signer0, other] = await ethers.getSigners();
    const allsigners = await ethers.getSigners();


    await expect(
        TestPathRInContract.connect(other).addToWhiteList(signer0.address)
    ).to.be.revertedWith(
        "Ownable: caller is not the owner"
    );

    for (let i = 0; i < 5; i++) {
      await TestPathRInContract.connect(allsigners[0]).addToWhiteList(allsigners[i].address);
    }

    // unauthorised guy try to delete
    await expect(
        TestPathRInContract.connect(other).removeFromWhiteList(signer0.address)
    ).to.be.revertedWith(
        "Ownable: caller is not the owner"
    );

    // authorised guy delete
    await TestPathRInContract.connect(allsigners[0]).removeFromWhiteList(0);

    // get all the white listed accounts
    const whiteListed = await TestPathRInContract.getWhiteList();

    // check if the account is removed from the white list
    assert(!whiteListed.includes(signer0.address));


    //unauthorised guy try to delete
    await expect(
        TestPathRInContract.connect(other).removeFromWhiteList(signer0.address)
    ).to.be.revertedWith(
        "Ownable: caller is not the owner"
    );


    await TestPathRInContract.connect(allsigners[0]).removeFromWhiteList(1);
    await TestPathRInContract.connect(allsigners[0]).removeFromWhiteList(2);

    await TestPathRInContract.connect(allsigners[0]).removeFromWhiteList(0);
    await TestPathRInContract.connect(allsigners[0]).removeFromWhiteList(0);


    //assume to be empty array
    const whiteListed1 = await TestPathRInContract.getWhiteList();
    assert.strictEqual(whiteListed1.length, 0, 'Whitelist array is not empty');

    //now we add signer3
    await TestPathRInContract.connect(allsigners[0]).addToWhiteList(allsigners[3].address);
    const whiteListed3 = await TestPathRInContract.getWhiteList();
    assert.strictEqual(whiteListed3.length, 1, 'Whitelist array should be 1');
    assert(whiteListed.includes(allsigners[3].address));
  });




  it("stake permission", async () => {

    const [signer0, other] = await ethers.getSigners();
    const allsigners = await ethers.getSigners();


    await expect(
        TestPathRInContract.connect(other).addToWhiteList(signer0.address)
    ).to.be.revertedWith(
        "Ownable: caller is not the owner"
    );

    for (let i = 0; i < 2; i++) {
      await TestPathRInContract.connect(allsigners[0]).addToWhiteList(allsigners[i].address);
    }
    await hre.network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [USDC_WHALE],
    });

    const impersonateSigner = await ethers.getSigner(USDC_WHALE);
    const USDCContract = await ethers.getContractAt("IERC20", USDC);

    const user1Balance = await USDCContract.balanceOf(
        impersonateSigner.address
    );
    console.log(impersonateSigner.address);
    console.log(
        "Initial USDC Balance:",
        ethers.utils.formatUnits(user1Balance, 6)
    );


    forbiddenAccount=allsigners[3];
    // we send usdc to the 50 test account and approve for staking
    const signers = await ethers.getSigners();

      await USDCContract.connect(impersonateSigner).transfer(
          forbiddenAccount.address,
          2
      );

      await USDCContract.connect(forbiddenAccount).approve(
          TestPathRInContract.address,
          2
      );

    await expect(TestPathRInContract.connect(forbiddenAccount).stakeTokens(USDC, 1)).to.be.revertedWith("not in the relayer list");

    await TestPathRInContract.connect(allsigners[0]).addToWhiteList(forbiddenAccount.address);

    TestPathRInContract.connect(forbiddenAccount).stakeTokens(USDC, 1);

    await TestPathRInContract.connect(allsigners[0]).removeFromWhiteList(2);

    await expect(TestPathRInContract.connect(forbiddenAccount).stakeTokens(USDC, 1)).to.be.revertedWith("not in the relayer list");

  });



  it("should stake 8 accounts", async () => {
    await hre.network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [USDC_WHALE],
    });

    const impersonateSigner = await ethers.getSigner(USDC_WHALE);
    const USDCContract = await ethers.getContractAt("IERC20", USDC);

    const user1Balance = await USDCContract.balanceOf(
      impersonateSigner.address
    );
    console.log(impersonateSigner.address);
    console.log(
      "Initial USDC Balance:",
      ethers.utils.formatUnits(user1Balance, 6)
    );

    // we send usdc to the 50 test account and approve for staking
    const signers = await ethers.getSigners();

    for (let i = 0; i < 8; i++) {
      await USDCContract.connect(impersonateSigner).transfer(
        signers[i].address,
        100
      );

      await USDCContract.connect(signers[i]).approve(
        TestPathRInContract.address,
        100
      );
      await TestPathRInContract.connect(signers[0]).addToWhiteList(signers[i].address);
    }

    await TestPathRInContract.connect(signers[0]).stakeTokens(USDC, 1);
    await TestPathRInContract.connect(signers[1]).stakeTokens(USDC, 2);


    assert((await TestPathRInContract.getStakeBalance(signers[0].address)) == 1);
    assert((await TestPathRInContract.getStakeBalance(signers[1].address)) == 2);


    await TestPathRInContract.connect(signers[3]).stakeTokens(USDC, 8);

    var top_accounts = await TestPathRInContract.getTopAccounts();

    console.log(">>>>>top_accounts", top_accounts[0].addr);

    assert(top_accounts[0].addr == signers[3].address);
    assert(top_accounts[1].addr == signers[1].address);
    assert(top_accounts[2].addr == signers[0].address);
    //
    //
    await TestPathRInContract.connect(signers[0]).stakeTokens(USDC, 4);

    top_accounts = await TestPathRInContract.getTopAccounts();
    // check the top account
    console.log(">>>>>top_accounts", top_accounts[0].addr);

    assert(top_accounts[0].addr.toString() == signers[3].address.toString());
    assert(top_accounts[1].addr.toString() == signers[0].address.toString());
    assert(top_accounts[2].addr.toString() == signers[1].address.toString());

   // signer3 withdrawal
    await  TestPathRInContract.connect(signers[3]).unstakeTokens(USDC, 7);
    expect(await TestPathRInContract.getStakeBalance(signers[3].address)).to.equal(1);


    top_accounts = await TestPathRInContract.getTopAccounts();
    console.log(">>>>>top_accounts", top_accounts);


    assert(top_accounts[2].addr.toString() == signers[3].address.toString(),"signer3 should be in the 2 account");
    assert(top_accounts[0].addr.toString() == signers[0].address.toString(),"signer0 should be in the 0 account");
    assert(top_accounts[1].addr.toString() == signers[1].address.toString(),"signer1 should be in the 1 account");


    await  TestPathRInContract.connect(signers[3]).unstakeTokens(USDC, 1);
    expect(await TestPathRInContract.getStakeBalance(signers[3].address)).to.equal(0);


   // expect the error to be Insufficient token balance, try lesser amount
    await expect(
     TestPathRInContract.connect(signers[3]).unstakeTokens(USDC, 1),
    ).to.be.revertedWith("Insufficient token balance, try lesser amount");

    // now we have 8 nodes staking

    for (let i = 0; i < 8; i++) {
      await TestPathRInContract.connect(signers[i]).stakeTokens(USDC, 20+i);
    }

    top_accounts = await TestPathRInContract.getTopAccounts();
    console.log(">>>>>top_accounts", top_accounts);

    assert(top_accounts[0].addr.toString() == signers[7].address.toString(),"signer7 should be in the 0 account");
    assert(top_accounts.length==5,"top list length should be 5");


  });



  it("test choose the relayer", async () => {
    await hre.network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [USDC_WHALE],
    });

    const impersonateSigner = await ethers.getSigner(USDC_WHALE);
    const USDCContract = await ethers.getContractAt("IERC20", USDC);

    const user1Balance = await USDCContract.balanceOf(
        impersonateSigner.address
    );
    console.log(impersonateSigner.address);
    console.log(
        "Initial USDC Balance:",
        ethers.utils.formatUnits(user1Balance, 6)
    );

    // we send usdc to the 50 test account and approve for staking
    const signers = await ethers.getSigners();

    for (let i = 0; i < 8; i++) {
      await USDCContract.connect(impersonateSigner).transfer(
          signers[i].address,
          100
      );

      await USDCContract.connect(signers[i]).approve(
          TestPathRInContract.address,
          100
      );
      await TestPathRInContract.connect(signers[0]).addToWhiteList(signers[i].address);
    }

    await TestPathRInContract.connect(signers[0]).stakeTokens(USDC, 1);
    await TestPathRInContract.connect(signers[1]).stakeTokens(USDC, 2);


    assert((await TestPathRInContract.getStakeBalance(signers[0].address)) == 1);
    assert((await TestPathRInContract.getStakeBalance(signers[1].address)) == 2);


    await TestPathRInContract.connect(signers[3]).stakeTokens(USDC, 8);

    var top_accounts = await TestPathRInContract.getTopAccounts();


    assert(top_accounts[0].addr == signers[3].address);
    assert(top_accounts[1].addr == signers[1].address);
    assert(top_accounts[2].addr == signers[0].address);

    // get current block height
    var blockHeight = await ethers.provider.getBlockNumber();

    console.log(">>>>>blockHeight", blockHeight);
    var choose = parseInt(blockHeight%1000/100%top_accounts.length);

    console.log(">>chosen relayer", choose);
    const firstchoose=choose;

    var relayer = await TestPathRInContract.getCurrentRelayer();
    expect(relayer).to.equal(top_accounts[choose].addr);

    // now we test all the other relayers
    await hre.network.provider.send("hardhat_mine", ["100"]);

    blockHeight = await ethers.provider.getBlockNumber();
    console.log(">>>>>blockHeight", blockHeight);

    choose = parseInt(blockHeight%1000/100%top_accounts.length);
    console.log(">>chosen relayer", choose);

    var relayer = await TestPathRInContract.getCurrentRelayer();
    expect(relayer).to.equal(top_accounts[(firstchoose+1)%top_accounts.length].addr);


    // now we test all the other relayers
    await hre.network.provider.send("hardhat_mine", ["100"]);

    blockHeight = await ethers.provider.getBlockNumber();
    console.log(">>>>>blockHeight", blockHeight);

    choose = parseInt(blockHeight%1000/100%top_accounts.length);
    console.log(">>chosen relayer", choose);

    var relayer = await TestPathRInContract.getCurrentRelayer();
    expect(relayer).to.equal(top_accounts[choose].addr);


    // now we test all the other relayers
    await hre.network.provider.send("hardhat_mine", ["100"]);

    blockHeight = await ethers.provider.getBlockNumber();
    console.log(">>>>>blockHeight", blockHeight);

    choose = parseInt(blockHeight%1000/100%top_accounts.length);
    console.log(">>chosen relayer", choose);

    var relayer = await TestPathRInContract.getCurrentRelayer();
    expect(relayer).to.equal(top_accounts[choose].addr);

  });

});

